# Stage 1 Etching

Neck length = 220-240 um

Neck diameter = 230-240 um

Neck top to thinnest point distance = 380-400 um

End length = 1850-2000 um


# Stage 2 Etching

Neck length = 220-240 um

Neck diameter = 230-240 um

Tip length < 500 um

Tip diameter = 10 nm